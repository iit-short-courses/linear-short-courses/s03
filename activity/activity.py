import queue


class PriorityLane(): 
    def __init__(self):
        self.my_list = []
    
    # methods
    def enqueue(self, list_item):
        return self.my_list.insert(0, list_item)
    def dequeue(self):
        return self.my_list.pop()
    def is_empty(self):
        if(len(self.my_list) == 0):
            return True
        else:
            return False
    def peek(self):
        return self.my_list[-1]
    def size(self):
        return len(self.my_list)
    def print(self):
        print(self.my_list)
    
security = PriorityLane()

security.enqueue('senior')
security.enqueue('pregnant')
security.enqueue('PWD')
security.enqueue('normal person')

print("Original queue list items:")
security.print()
print("")
print(f"Is the security list empty? {security.is_empty()}\n")
print(f"The current size of the security list: {security.size()}\n")
print(f"Peek of the new list queue item: {security.peek()}\n")
print(f"The item is removed from the security list: {security.dequeue()}\n")
